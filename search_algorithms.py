import matplotlib.pyplot as plt
import numpy as np
import random
import string

# Алгоритмы поиска
def lin_search(arr, target):
    operations = 0
    if not isinstance(arr, list):
        raise TypeError('arg \'arr\' must be list')
    if isinstance(target, float) or isinstance(target, int):
        for i in range(len(arr)):
            operations += 1
            if arr[i] == target:
                return i, operations
        return -1, operations
    else:
        raise TypeError('arg \'target\' must be float or int')

def bin_search(arr, target):
    operations = 0
    if not isinstance(arr, list):
        raise TypeError('arg \'arr\' must be list')
    if not all(arr[i] <= arr[i+1] for i in range(len(arr) - 1)):
        raise TypeError('arr must be asc sorted array')
    if isinstance(target, float) or isinstance(target, int):
        a = 0
        b = len(arr) - 1
        while a <= b:
            operations += 1
            i = (a + b)//2
            if arr[i] < target:
                a = i + 1
            elif arr[i] > target:
                b = i - 1
            else:
                return i, operations
        return -1, operations
    else:
        raise TypeError('arg \'target\' must be float or int')

def prefix(text):
    prefs = [0] * len(text)
    for i in range(1, len(text)):
        j = prefs[i - 1]
        while j > 0 and text[i] != text[j]:
            j = prefs[j - 1]
        if text[i] == text[j]:
            j += 1
        prefs[i] = j
    return prefs

def naive_search(text, target):
    operations = 0
    if not len(target):
        raise TypeError('target have length 0')
    if not (isinstance(text, str) and isinstance(target, str)):
        raise TypeError('arg must be string')
    res = []
    j = 0
    for i in range(len(text) - len(target) + 1):
        while j < len(target) and text[i + j] == target[j]:
            operations += 1
            j += 1
        if j == len(target):
            res.append(i)
        j = 0
    return res, operations

def KMP_search(text, target):
    operations = 0
    if not (isinstance(text, str) and isinstance(target, str)):
        raise TypeError('arg must be string')
    prefs = prefix(target)
    res = []
    j = 0
    for i in range(len(text)):
        while j == len(target) or (j > 0 and target[j] != text[i]):
            operations += 1
            j = prefs[j - 1]
            if len(target) - j > len(text) - i:
                break
        if text[i] == target[j]:
            j += 1
            operations += 1
        if j == len(target):
            res.append(i - j + 1)
    return res, operations

# Генерация массива
def generate_array(n):
    return [random.randint(0, n) for _ in range(n)]

# Генерация строки
def generate_string(n):
    return ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase, k=n))

sizes = list(np.logspace(1, 5, num=5, dtype=int))

# Построение графиков
fig, axs = plt.subplots(2, 2)

# График линейного поиска
operations = []
for size in sizes:
    data = generate_array(size)
    _, ops = lin_search(data, data[-1])
    operations.append(ops)

axs[0, 0].plot(sizes, operations)
axs[0, 0].set_title('Линейный поиск')

# График бинарного поиска
operations = []
for size in sizes:
    data = sorted(generate_array(size))
    _, ops = bin_search(data, data[-1])
    operations.append(ops)

axs[0, 1].plot(sizes, operations)
axs[0, 1].set_title('Бинарный поиск')

# График наивного поиска
operations = []
for size in sizes:
    data = generate_string(size)
    _, ops = naive_search(data, data[-1])
    operations.append(ops)

axs[1, 0].plot(sizes, operations)
axs[1, 0].set_title('Наивный поиск')

# График поиска Кнута-Морриса-Пратта
operations = []
for size in sizes:
    data = generate_string(size)
    _, ops = KMP_search(data, data[-1])
    operations.append(ops)

axs[1, 1].plot(sizes, operations)
axs[1, 1].set_title('Поиск Кнута-Морриса-Пратта')

for ax in axs.flat:
    ax.set(xlabel='Размерность', ylabel='Количество операций')

# Hide x labels and tick labels for top plots and y ticks for right plots.
for ax in axs.flat:
    ax.label_outer()

plt.tight_layout()
plt.show()
