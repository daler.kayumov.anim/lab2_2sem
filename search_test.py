import pytest
from search_algorithms import lin_search, bin_search, naive_search, KMP_search

test_lin_data = [
    [4, 7, 1, 9, 3, 6, 2, 4, 5, 0],
    [9, 7, 9, 0],
    [],
]
test_lin_targets = [5, 9, 1]
test_lin_expected = [8, 0, -1]

test_bin_data = [
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    [1, 7, 9, 9],
    [],
]
test_bin_targets = [5, 9, 1]
test_bin_expected = [5, 2, -1]

test_strings_data = [
    'abobaboba',
    'aboba',
    ''
]
test_strings_target = 'bob'
test_strings_expected = [[1, 5], [1], []]   # Обновленные ожидаемые результаты

@pytest.mark.parametrize('data, target, expected', zip(test_lin_data, test_lin_targets, test_lin_expected))
def test_lin_search(data, target, expected):
    assert lin_search(data, target)[0] == expected

@pytest.mark.parametrize('data, target, expected', zip(test_bin_data, test_bin_targets, test_bin_expected))
def test_bin_search(data, target, expected):
    assert bin_search(data, target)[0] == expected

@pytest.mark.parametrize('data, expected', zip(test_strings_data, test_strings_expected))
def test_naive_search(data, expected):
    assert naive_search(data, test_strings_target)[0] == expected

@pytest.mark.parametrize('data, expected', zip(test_strings_data, test_strings_expected))
def test_KMP_search(data, expected):
    assert KMP_search(data, test_strings_target)[0] == expected
